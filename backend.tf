terraform {
  backend "gcs" {
    bucket = "project-factory-terraform-state"
    prefix = "terraform/state"
  }
}
