// get gitlab token for authentication
const {GITLAB_TOKEN} = process.env;

// load output.json from previous step
const outputs = require("../output.json");

const axios = require('axios').default

const fs = require("fs/promises")

const gitlab = axios.create({
    baseURL: "https://gitlab.com/api/v4",
    headers: {
        'Authorization': `Bearer ${GITLAB_TOKEN}`
    }
});

(async () => {
    for(project in outputs.projects.value){
        loadProjectTemplateCode(outputs.projects.value[project]);
    }
})();

async function loadProjectTemplateCode(project){
    const {gitlab_project_id, terraform_state_bucket_name} = project;
    try {
        console.log('Listing template repository files')
        
        const templateFiles = await fs.readdir("./project-template-files");

        const actions = [];

        for (const file of templateFiles) {
            console.log(`Loading file ${file}`);
            const fileContent = await fs.readFile(`./project-template-files/${file}`, {encoding: 'UTF-8'});
            // replace placeholders
            const finalContent = fileContent
                    .replace(/%bucket_name%/g, terraform_state_bucket_name);

            const action = {
                action: 'create',
                file_path: file,
                content: finalContent,
            }
            actions.push(action);
        }
        console.log('Creating commit')

        await createCommit(gitlab_project_id, actions, '🎉 : init project from template')
    } catch (error) {
        console.error(error.message);
    }
}

async function createCommit(targetProjectId, actions, commitMessage) {
    await gitlab.post(`projects/${targetProjectId}/repository/commits`,
        {
            branch: 'main',
            commit_message: commitMessage,
            actions,
        })
}
