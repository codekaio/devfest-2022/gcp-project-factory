module "gcp-projects" {
  source  = "./project"

  for_each = toset(var.projects)

  project_name = each.value

  org_id          = var.org_id
  billing_account = var.billing_account
}