variable "billing_account" {
    type = string
}

variable "org_id" {
    type = string
}

variable "projects" {
    type = list(string)
}
