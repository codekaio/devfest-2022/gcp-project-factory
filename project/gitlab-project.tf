data "gitlab_group" "group" {
  full_path = "codekaio/devfest-2022"
}

resource "gitlab_project" "this" {
  name                   = var.project_name
  visibility_level       = "private"
  namespace_id           = data.gitlab_group.group.id
  shared_runners_enabled = true
}

resource "google_service_account_key" "key" {
  service_account_id = module.gcp-project.service_account_email
}

resource "gitlab_project_variable" "ci_variable" {
  project       = gitlab_project.this.id
  key           = "GOOGLE_CREDENTIALS"
  value         = base64decode(google_service_account_key.key.private_key)
  variable_type = "file"
}

resource "gitlab_project_variable" "google_project_variable" {
  project = gitlab_project.this.id
  key     = "GOOGLE_PROJECT"
  value   = module.gcp-project.project_id
}
