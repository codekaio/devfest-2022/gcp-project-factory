locals {
  activate_apis = [
    "compute.googleapis.com",
    "storage-component.googleapis.com",
    "pubsub.googleapis.com",
  ]

  roles = [
    "roles/storage.admin",
    "roles/resourcemanager.projectIamAdmin"
  ]
}

module "gcp-project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 13.0"

  name = var.project_name
  random_project_id = true

  org_id          = var.org_id
  billing_account = var.billing_account

  activate_apis = local.activate_apis

  auto_create_network = true

  bucket_name     = "terraform-state-${var.project_name}"
  bucket_project  = var.project_name
  bucket_location = "eu"

  project_sa_name = "terraform"
}

resource "google_project_iam_member" "terraform_service_account_roles" {
  for_each = toset(local.roles)

  project = module.gcp-project.project_id
  role    = each.value
  member  = "serviceAccount:${module.gcp-project.service_account_email}"
}