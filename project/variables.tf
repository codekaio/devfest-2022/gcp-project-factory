variable "billing_account" {
    type = string
}

variable "org_id" {
    type = string
}

variable "project_name" {
    type = string
}
