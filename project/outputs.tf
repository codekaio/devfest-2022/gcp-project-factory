output "project_name" {
    value = var.project_name
}

output "gitlab_project_id" {
    value = gitlab_project.this.id
}

output "terraform_state_bucket_name" {
    value = "terraform-state-${var.project_name}"
}