terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
    }
  }
}